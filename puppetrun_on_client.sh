#!/bin/sh
. ./config.sh
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -l root `docker inspect -f "{{.NetworkSettings.IPAddress}}" ${COMPOSENAME}_client_1` 'puppet_apply.sh'
