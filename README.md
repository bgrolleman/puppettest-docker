# Puppet Test Environment

This uses docker to test puppetruns on a local environment. While not exactly
like production it's a quick way to test changes before submitting them.

## Quick Start Guide

### Software needed

* docker
* docker-compose

### Code needed

Make sure you have a clone of the puppet modules in ./puppet/modules and a
clone of the hiera configuration in ./puppet/hiera. 

### Build/Start the docker image

Run the rebuild\_compose.sh script

### Do a test puppet run

Run the ./puppetrun\_on\_client.sh script and examine results of the puppetrun.

### Examine Results

Run the ./ssh\_to\_client.sh script to login to the box and look around

## Notes

### Not storing known hosts information
Because docker generates new containers all the time, it's not usefull to keep
track of host keys so we send them to /dev/null

### Setup SSH Keys
You need to add your public key to the keys/authorized_keys file
